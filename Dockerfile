FROM python:3.10-slim AS build
RUN pip install --no-cache-dir mkdocs
WORKDIR /mkdocs
COPY mkdocs.yml .
COPY docs docs
COPY theme theme
RUN mkdocs build --strict --verbose --site-dir /site

FROM nginx
WORKDIR /usr/share/nginx/html
COPY --from=build /site .
