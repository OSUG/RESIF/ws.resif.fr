# RESIFSI/orphanfile


| Service interface                                     | Version | Summary                                     | Return options |
|-------------------------------------------------------|---------|---------------------------------------------|----------------|
| [orphanfile](/resifsi/orphanfile/1) | v1      | Getting details about data without metadata | JSON           |
    
