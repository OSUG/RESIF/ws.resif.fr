# RESIFSI/transaction


| Service interface                                        | Version | Summary                                             | Return options |
|----------------------------------------------------------|---------|-----------------------------------------------------|----------------|
| [transaction](/resifsi/transaction/1) | v1      | Getting integration transactions status and details | JSON           |

