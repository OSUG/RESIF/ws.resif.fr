# RESIFSI/assembleddata

| Service interface                                           | Version | Summary                                 | Return options                                                      |
|-------------------------------------------------------------|---------|-----------------------------------------|---------------------------------------------------------------------|
| [assembleddata](/resifsi/assembleddata/1) | v1      | Getting static preassembled events data | [miniSEED](https://ds.iris.edu/ds/nodes/dmc/data/formats/miniseed/) |

