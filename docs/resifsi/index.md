# Résif-DC Web services

Welcome to the Résif-DC webservices endpoint. More information about Résif seismological data can be found at [https://seismology.resif.fr](https://seismology.resif.fr)

For questions about accessing data, filling bug reports, making suggestions, asking for help, please visit our helpdesk  [https://gitlab.com/resif/sismo-help](https://gitlab.com/resif/sismo-help) or send an email to  <sismo-help@resif.fr>

!!! note
    These services may be used under Résif Data Services [Terms of Service](https://seismology.resif.fr/data-policy).
    Usage of the services and data in publications should cite the services according to our [citation instructions](https://seismology.resif.fr/citation-guidelines/) and [data by network](http://www.fdsn.org/networks/citation/).


## RESIF-SI Web services

Web services for internal use within Résif-SI.


| Service interface                      | Version | Summary                                             | Return options                                                      |
|----------------------------------------|---------|-----------------------------------------------------|---------------------------------------------------------------------|
| [transaction](transaction)     | v1      | Getting integration transactions status and details | JSON                                                                |
| [orphanfile](orphanfile)       | v1      | Getting details about data without metadata         | JSON                                                                |
| [assembleddata](assembleddata) | v1      | Getting static preassembled events data             | [miniSEED](https://ds.iris.edu/ds/nodes/dmc/data/formats/miniseed/) |
