# RESIFWS/resp

| Service interface       | Versions | Summary                      | Return options                                                         |
|-------------------------|----------|------------------------------|------------------------------------------------------------------------|
| [resp](/resifws/resp/1) | v1       | channel response information | [SEED RESP](http://ds.iris.edu/ds/support/faq/16/what-is-a-resp-file/) |

