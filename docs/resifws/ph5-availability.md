# RESIFWS/ph5-availability


| Service interface                                                 | Versions | Summary                                         | Return options                                                      |
|-------------------------------------------------------------------|----------|-------------------------------------------------|---------------------------------------------------------------------|
| [ph5-availability](http://ph5ws.resif.fr/resifws/availability/1/) | v1       | time series data availability for the PH5 store | text, [GeoCSV](http://geows.ds.iris.edu/documents/GeoCSV.pdf), json |
