# RESIFWS/sacpz

| Service interface                           | Versions | Summary                                       | Return options |
|---------------------------------------------|----------|-----------------------------------------------|----------------|
| [sacpz](/resifws/sacpz/1) | v1       | instrument response information (per channel) | ASCII          |

