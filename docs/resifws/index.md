# Résif-DC Web services

Welcome to the Résif-DC webservices endpoint. More information about Résif seismological data can be found at [https://seismology.resif.fr](https://seismology.resif.fr)

For questions about accessing data, filling bug reports, making suggestions, asking for help, please visit our helpdesk  [https://gitlab.com/resif/sismo-help](https://gitlab.com/resif/sismo-help) or send an email to  <sismo-help@resif.fr>

!!! note
    These services may be used under Résif Data Services [Terms of Service](https://seismology.resif.fr/data-policy).
    Usage of the services and data in publications should cite the services according to our [citation instructions](https://seismology.resif.fr/citation-guidelines/) and [data by network](http://www.fdsn.org/networks/citation/).

## Résif Web services

The webservices developped and maintained by the Résif-DC team.

| Service interface                    | Versions | Summary                                                                                       | Return options                                                         |
|--------------------------------------|----------|-----------------------------------------------------------------------------------------------|------------------------------------------------------------------------|
| [timeseries](timeseries)             | v1       | similar to ws-dataselect with additional options for processing and reformatting              | ASCII, WAV, miniSEED, SAC, PNG                                         |
| [timeseriesplot](timeseriesplot)     | v1       | A charting webservice offering timeseries graphic display in single-line or helicorder styles | image: PNG (default) or JPEG                                           |
| [sacpz](sacpz)                       | v1       | instrument response information (per channel)                                                 | ASCII                                                                  |
| [resp](resp)                         | v1       | channel response information                                                                  | [SEED RESP](http://ds.iris.edu/ds/support/faq/16/what-is-a-resp-file/) |
| [evalresp](evalresp)                 | v1       | instrument response information evaluated from Résif metadata                                 | ASCII, Bode style plot                                                 |
| [ppsd](ppsd)                         | v1       | Check on power spectral densities (PSD)                                                       | PNG, NPZ, ASCII                                                        |
| [statistics](statistics)             | v1       | Résif-DC statistics                                                                           | ASCII, PNG                                                             |
| [ph5-availability](ph5-availability) | v1       | time series data availability for the PH5 store                                               | text, [GeoCSV](http://geows.ds.iris.edu/documents/GeoCSV.pdf), json    |


