# RESIFWS/seedpsd

| Service interface             | Versions | Summary                                     | Return options          |
|-------------------------------|----------|---------------------------------------------|-------------------------|
| [seedpsd](/resifws/seedpsd/1) | v1       | Querying Power Spectral Density information | PDF, spectrograms, text |

