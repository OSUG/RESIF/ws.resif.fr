# RESIFWS/timeseriesplot


| Service interface                                             | Versions | Summary                                                                                       | Return options               |
|---------------------------------------------------------------|----------|-----------------------------------------------------------------------------------------------|------------------------------|
| [timeseriesplot](/resifws/timeseriesplot/1) | v1       | A charting webservice offering timeseries graphic display in single-line or helicorder styles | image: PNG (default) or JPEG |

