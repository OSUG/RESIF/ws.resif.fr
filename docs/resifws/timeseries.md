# RESIFWS/timeseries

| Service interface                              | Versions | Summary                                                                          | Return options                 |
|------------------------------------------------|----------|----------------------------------------------------------------------------------|--------------------------------|
| [timeseries](/resifws/timeseries/1) | v1       | similar to ws-dataselect with additional options for processing and reformatting | ASCII, WAV, miniSEED, SAC, PNG |

