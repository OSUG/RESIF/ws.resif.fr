# RESIFWS/statistics

    
| Service interface                            | Versions | Summary             | Return options |
|----------------------------------------------|----------|---------------------|----------------|
| [statistics](/resifws/statistics/1) | v1       | Résif-DC statistics | ASCII, PNG     |
