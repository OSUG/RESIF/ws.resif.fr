# Dataselect requests using EIDA token for authentication

EIDA provides an authentication service delivering a temporary token to the user.

Users need to submit their token first in order to receive temporary login and password to pass to their dataselect request.

Full documentation is provided on the [EIDA users feedback page](https://github.com/EIDA/userfeedback/#eida-authentication-system--accessing-restricted-data)


| Service interface                                            | Versions | Summary                        | Return options |
|--------------------------------------------------------------|----------|--------------------------------|----------------|
| [EIDA data access authentication](/fdsnws/dataselect/1/auth) | v1       | EIDA authentication with token | text           |

