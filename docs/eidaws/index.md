# Résif-DC Web services

Welcome to the Résif-DC webservices endpoint. More information about Résif seismological data can be found at [https://seismology.resif.fr](https://seismology.resif.fr)

For questions about accessing data, filling bug reports, making suggestions, asking for help, please visit our helpdesk  [https://gitlab.com/resif/sismo-help](https://gitlab.com/resif/sismo-help) or send an email to  <sismo-help@resif.fr>

!!! note
    These services may be used under Résif Data Services [Terms of Service](https://seismology.resif.fr/data-policy).
    Usage of the services and data in publications should cite the services according to our [citation instructions](https://seismology.resif.fr/citation-guidelines/) and [data by network](http://www.fdsn.org/networks/citation/).


## EIDA Web services

Common webservices accross [EIDA nodes](https://www.orfeus-eu.org/data/eida/webservices/).

| Service interface                                  | Versions | Summary                                                                                    | Return options |   |
|----------------------------------------------------|----------|--------------------------------------------------------------------------------------------|----------------|---|
| [Waveform Catalog](wfcatalog)                      | v1       | Provides access to metadata and quality parameters for archives contributing data to EIDA. | json           |   |
| [EIDA data access authentication](dataselect-auth) | v1       | EIDA authentication with token                                                             | text           |   |
| [EIDA usage statistics](statistics/1/)      | v1       | Provides unified statistics about data delivery within EIDA federation                     | json,csv       |   |
