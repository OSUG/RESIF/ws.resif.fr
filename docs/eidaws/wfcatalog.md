# EIDAWS/wfcatalog

The WFCatalog Webservice provides detailed information on the contents of waveform data including quality control parameters. Information can be included on sample metrics, record header flags, and timing quality. The WFCatalog can serve as an index for data discovery as it has support for range filtering on all available metrics.

To help the user manipulate this service, an [URL builder](https://seismology.resif.fr/wfcatalogue/#/) is provided by Résif-DC.


| Service interface                                         | Versions | Summary                                                                                    | Return options |
|-----------------------------------------------------------|----------|--------------------------------------------------------------------------------------------|----------------|
| [Waveform Catalog](/eidaws/wfcatalog/1) | v1       | Provides access to metadata and quality parameters for archives contributing data to EIDA. | json           |
