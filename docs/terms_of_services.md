# Terms of usage for Résif-DC services 

Data services from Résif-DC are subject to a fair-use policy.

Users may use our services as long as client actions do not inhibit our capability to serve others. 

## WebService usage policy

Web services hosted at the [Résif-DC](https://ws.resif.fr) should be used within some limitations.


Résif-DC may refuse to honor the requests that goes beyond this limit by returning the http code `429`

NOTE: In particular, polling for real-time continuous time series data is not allowed on any webservice. For continuous, near real-time data please use our [streaming service](https://seismology.resif.fr/real-time-seedlink/).

## Realtime data stream service usage policy

Résif-DC allows a maximum of 5 active connections for a user Résif-DC may cut arbitrary the connections beyond that limit.


## About personal information management

When accessing services hosted by Résif-DC, the client's IP address is collected and stored in a database for operational purposes. It allows the service operators to:

  - be able to analyze the overall activity
  - build anonimized statistics about data usage, by counting distinct IP addresses. Statistics are publicly available through the 

The IP addresses are kept 65 days. After that period, the information is deleted automatically.

If you want to know more about our statistics process, you can contact us at [mailto:sismo-help@resif.fr](mailto:sismo-help@resif.fr)
## What to do in case of error 429 ?

This error is returned by our webserver to the clients issuing to many requests in a time span. If you think that you should be able to issue more requests, please contact us at [mailto:sismo-help@resif.fr](mailto:sismo-help@resif.fr)

<!-- ### Limits -->
<!-- Limitations apply by IP addresses. -->

<!-- - Users should limit usage to no more than 120 requests per seconds to [fdsnws-dataselect](../fdsnws/dataselect) or [fdsnws-station](../fdsnws/station) webservices. -->
<!-- - Users should limit usage to no more than 30 requests per seconds to webservices involving distant computations : -->
<!--   - [ph5-dataselect](../fdsnws/ph5-dataselect) -->
<!--   - [timeseries](../resifws/timeseries) -->
<!--   - [timeseriesplot](../resifws/timeseriesplot) -->
<!-- - Users should limit usage to no more than 10 requests per seconds to webservices involving distant computations : -->
<!--   - [seedpsd](../resifws/seedpsd) -->
  
<!-- After 60 seconds of inactivity, the client is allowed again. -->

