# Clients

## Command line fetch script

| Script        | Files                                                                            | Description                                                                                                 |
|---------------|----------------------------------------------------------------------------------|-------------------------------------------------------------------------------------------------------------|
| FetchEvent    | [Download](https://seiscode.iris.washington.edu/projects/ws-fetch-scripts/files) | Fetch event (earthquake) information in ASCII text format, source XML can also be saved                     |
| FetchData     | [Download](https://seiscode.iris.washington.edu/projects/ws-fetch-scripts/files) | Fetch time series data (miniSEED), simple metadata (ASCII) and instrument responses (SEED RESP and SAC PZs) |
| FetchMetadata | [Download](https://seiscode.iris.washington.edu/projects/ws-fetch-scripts/files) | Fetch basic time series channel metadata in ASCII text format, source XML can also be saved                 |

## ObsPy/Python data access and processing

The [ObsPy](http://www.obspy.org) project is dedicated to provide a Python framework for processing seismological data. Included in this framework is the capability to [retrieve data from FDSN Web services](http://docs.obspy.org/packages/obspy.fdsn.html) (among many other data sources).

If you wish to retrieve data from the DMC and process it in Python using ObsPy is highly recommended.

## SeisIO.jl/Julia - A package for working with geophysical time series data

A minimalist, platform-agnostic package for working with geophysical time series data in the Julia language. Supports accessing data via Web services in addition to reading common seismic data formats (SAC, miniSEED, SEGY).

Download the package and read the documentation from the [SeisIO project site](https://github.com/jpjones76/SeisIO.jl)

    
