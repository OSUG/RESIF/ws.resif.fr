# FDSNWS/event

There is also a [human friendly user interface](https://api.franceseisme.fr/fr/search) to the event webservice.

| Service interface                                  | Versions | Summary                                                                                                                            | Return options                                                                                          |
|----------------------------------------------------|----------|------------------------------------------------------------------------------------------------------------------------------------|---------------------------------------------------------------------------------------------------------|
| [event](http://api.franceseisme.fr/fdsnws/event/1) | v1       | contributed earthquake origin and magnitude estimates. **This service is hosted by [France Seisme](https://www.franceseisme.fr/)** | [QuakeML](https://quake.ethz.ch/quakeml), text, [GeoCSV](http://geows.ds.iris.edu/documents/GeoCSV.pdf) |

