# FDSNWS/station

There is also a [human friendly user interface](https://seismology.resif.fr/station-url-builder/) to this webservice.

| Service interface                              | Versions | Summary                                        | Return options                                               |
|------------------------------------------------|----------|------------------------------------------------|--------------------------------------------------------------|
| [station](/fdsnws/station/1) | v1       | metadata for time series stored in SEED format | [FDSN stationXML1.1](http://www.fdsn.org/xml/station/), text |

