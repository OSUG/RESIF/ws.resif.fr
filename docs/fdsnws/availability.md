# FDSNWS/availability

There is also a [human friendly user interface](https://seismology.resif.fr/url-builder-availability-v-1) to this webservice.

| Service interface                                        | Versions | Summary                       | Return options                                                               |
|----------------------------------------------------------|----------|-------------------------------|------------------------------------------------------------------------------|
| [availability](/fdsnws/availability/1) | v1       | time series data availability | text, [GeoCSV](http://geows.ds.iris.edu/documents/GeoCSV.pdf), json, Request |
