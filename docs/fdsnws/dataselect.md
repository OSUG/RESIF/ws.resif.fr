# FDSNWS/dataselect

There is also a [human friendly user interface](https://seismology.resif.fr/dataselect/) to this webservice.

| Service interface                                    | Versions | Summary                                        | Return options                                                                                                                                                                                          |
|------------------------------------------------------|----------|------------------------------------------------|---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| [dataselect](/fdsnws/dataselect/1) | v1       | time series data in miniSEED and other formats | [miniSEED](https://ds.iris.edu/ds/nodes/dmc/data/formats/miniseed/),  [SAC zip](https://ds.iris.edu/files/sac-manual/manual/file_format.html),  [GeoCSV](http://geows.ds.iris.edu/documents/GeoCSV.pdf) |

