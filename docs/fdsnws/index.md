# Résif-DC Web services

Welcome to the Résif-DC webservices endpoint. More information about Résif seismological data can be found at [https://seismology.resif.fr](https://seismology.resif.fr)

For questions about accessing data, filling bug reports, making suggestions, asking for help, please visit our helpdesk  [https://gitlab.com/resif/sismo-help](https://gitlab.com/resif/sismo-help) or send an email to  <sismo-help@resif.fr>

!!! note
    These services may be used under Résif Data Services [Terms of Service](https://seismology.resif.fr/data-policy).
    Usage of the services and data in publications should cite the services according to our [citation instructions](https://seismology.resif.fr/citation-guidelines/) and [data by network](http://www.fdsn.org/networks/citation/).

## FDSN Web services

Those are all FDSN standard webservices (http://www.fdsn.org/webservices/).

| Service interface                | Versions | Summary                                                                                                                            | Return options                                                                                                                                                                                         |
|----------------------------------|----------|------------------------------------------------------------------------------------------------------------------------------------|--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| [station](station)               | v1       | metadata for time series stored in SEED format                                                                                     | [FDSN stationXML1.1](http://www.fdsn.org/xml/station/), text                                                                                                                                           |
| [dataselect](dataselect)         | v1       | time series data in miniSEED and other formats                                                                                     | [miniSEED](https://ds.iris.edu/ds/nodes/dmc/data/formats/miniseed/),  [SAC zip](https://ds.iris.edu/files/sac-manual/manual/file_format.html),  [GeoCSV](http://geows.ds.iris.edu/documents/GeoCSV.pdf) |
| [event](event)                   | v1       | contributed earthquake origin and magnitude estimates. **This service is hosted by [France Seisme](https://www.franceseisme.fr/)** | [QuakeML](https://quake.ethz.ch/quakeml), text, [GeoCSV](http://geows.ds.iris.edu/documents/GeoCSV.pdf)                                                                                                |
| [availability](availability)     | v1       | time series data availability                                                                                                      | text, [GeoCSV](http://geows.ds.iris.edu/documents/GeoCSV.pdf), json, Request                                                                                                                           |
| [ph5-dataselect](ph5-dataselect) | v1       | time series data in miniSEED from PH5 archives                                                                                     | miniSEED                                                                                                                                                                                               |

## Overview

The Résif-DC’s Web services provide FDSN compliant access to a variety of information, including data and metadata from the IRIS DMC databases. FDSN Specifications can be found here

Web services can be accessed in a number of different ways:

  * Small requests can be handled through your browser.
  * [Dedicated clients](../clients.md) are available for making more complex or larger queries.
  * Various schemas are available for download.
  * Automated or scripted requests can be made using programs such as [wget](http://www.gnu.org/software/wget/) or [curl](http://curl.haxx.se/).

## Real time data

All open data arriving in (near) real time at Résif-DC are available from the [SeedLink server](https://seismology.resif.fr/real-time-seedlink/).

The Web services should not be used to retrieve continuous, real time data via repeated polling. Instead, the SeedLink server should be used when continuous data streams are needed.
