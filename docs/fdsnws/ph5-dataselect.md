# FDSNWS/dataselect for large-N achive


| Service interface                                           | Versions | Summary                                                 | Return options |
|-------------------------------------------------------------|----------|---------------------------------------------------------|----------------|
| [ph5-dataselect](http://ph5ws.resif.fr/fdsnws/dataselect/1) | v1       | time series data in miniSEED from large-N data archives | miniSEED       |
