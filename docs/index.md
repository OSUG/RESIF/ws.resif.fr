# Résif-DC Web services

Welcome to the Résif-DC webservices endpoint. More information about Résif seismological data can be found at [https://seismology.resif.fr](https://seismology.resif.fr)

For questions about accessing data, filling bug reports, making suggestions, asking for help, please visit our helpdesk  [https://gitlab.com/resif/sismo-help](https://gitlab.com/resif/sismo-help) or send an email to  <sismo-help@resif.fr>

!!! note
    These services may be used under Résif Data Services [Terms of Service](terms_of_services).
    Usage of the services and data in publications should cite the services according to our [citation instructions](https://seismology.resif.fr/citation-guidelines/) and [data by network](http://www.fdsn.org/networks/citation/).

## FDSN Web services

Those are all [FDSN standard webservices](http://www.fdsn.org/webservices/).

| Service interface                       | Versions | Summary                                                                                                                            | Return options                                                                                                                                                                                          | URL builder                                                              |
|-----------------------------------------|----------|------------------------------------------------------------------------------------------------------------------------------------|---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|--------------------------------------------------------------------------|
| [station](fdsnws/station)               | v1       | metadata for time series stored in SEED format                                                                                     | [FDSN stationXML1.1](http://www.fdsn.org/xml/station/), text                                                                                                                                            | [URL builder](https://seismology.resif.fr/station-url-builder/)          |
| [dataselect](fdsnws/dataselect)         | v1       | time series data in miniSEED and other formats                                                                                     | [miniSEED](https://ds.iris.edu/ds/nodes/dmc/data/formats/miniseed/),  [SAC zip](https://ds.iris.edu/files/sac-manual/manual/file_format.html),  [GeoCSV](http://geows.ds.iris.edu/documents/GeoCSV.pdf) | [URL builder](https://seismology.resif.fr/dataselect/)                   |
| [event](fdsnws/event)                   | v1       | contributed earthquake origin and magnitude estimates. **This service is hosted by [France Seisme](https://www.franceseisme.fr/)** | [QuakeML](https://quake.ethz.ch/quakeml), text, [GeoCSV](http://geows.ds.iris.edu/documents/GeoCSV.pdf)                                                                                                 | [URL builder](https://api.franceseisme.fr/en/search)                     |
| [availability](fdsnws/availability)     | v1       | time series data availability                                                                                                      | text, [GeoCSV](http://geows.ds.iris.edu/documents/GeoCSV.pdf), json, Request                                                                                                                            | [URL builder](https://seismology.resif.fr/url-builder-availability-v-1/) |
| [ph5-dataselect](fdsnws/ph5-dataselect) | v1       | time series data in miniSEED from PH5 archives                                                                                     | miniSEED                                                                                                                                                                                                |                                                                          |

## EIDA Web services

Common webservices accross [EIDA nodes](https://www.orfeus-eu.org/data/eida/webservices/).

| Service interface                                         | Versions | Summary                                                                                    | Return options | URL builder                                           |
|-----------------------------------------------------------|----------|--------------------------------------------------------------------------------------------|----------------|-------------------------------------------------------|
| [Waveform Catalog](eidaws/wfcatalog)                      | v1       | Provides access to metadata and quality parameters for archives contributing data to EIDA. | json           | [URL builder](https://seismology.resif.fr/wfcatalog/) |
| [EIDA data access authentication](eidaws/dataselect-auth) | v1       | EIDA authentication with token                                                             | text           |                                                       |
| [EIDA usage statistics](eidaws/statistics/1/)             | v1       | Provides unified statistics about data delivery within EIDA federation                     | json,csv       |                                                       |

## Résif Web services

The webservices developped and maintained by the Résif-DC team.

| Service interface                            | Versions | Summary                                                                                       | Return options                                                         |
|----------------------------------------------|----------|-----------------------------------------------------------------------------------------------|------------------------------------------------------------------------|
| [seedpsd](resifws/seedpsd)                   | v1       | Query the Power spectral densities of the data                                                | Histograms (PDF), spectrograms, text                                   |
| [timeseries](resifws/timeseries)             | v1       | similar to ws-dataselect with additional options for processing and reformatting              | ASCII, WAV, miniSEED, SAC, PNG                                         |
| [timeseriesplot](resifws/timeseriesplot)     | v1       | A charting webservice offering timeseries graphic display in single-line or helicorder styles | image: PNG (default) or JPEG                                           |
| [sacpz](resifws/sacpz)                       | v1       | instrument response information (per channel)                                                 | ASCII                                                                  |
| [resp](resifws/resp)                         | v1       | channel response information                                                                  | [SEED RESP](http://ds.iris.edu/ds/support/faq/16/what-is-a-resp-file/) |
| [evalresp](resifws/evalresp)                 | v1       | instrument response information evaluated from Résif metadata                                 | ASCII, Bode style plot                                                 |
| [statistics](resifws/statistics)             | v1       | Résif-DC statistics                                                                           | ASCII, PNG                                                             |
| [ph5-availability](resifws/ph5-availability) | v1       | time series data availability for the PH5 store                                               | text, [GeoCSV](http://geows.ds.iris.edu/documents/GeoCSV.pdf), json    |

## Résif-SI Web services

[Webservices](resifsi) for internal use within Résif-SI.

